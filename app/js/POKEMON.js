import {Pokemon} from "./classes/Pokemon";
import {Tools} from "./classes/Tools";

function POKEMON(){
    return [
        new Pokemon( 'Pikachu',      [3],        Tools.getRandomInt(10, 804),
            [0, 0],     "100",      "0" ),

        new Pokemon( 'Gobou',        [17],        Tools.getRandomInt(10, 967),
            [0, 0],     "-700",     "-1000" ),

        new Pokemon( 'Evoli',        [12],       Tools.getRandomInt(10, 918),
            [0, 0],     "-700",     "-500" ),

        new Pokemon( 'Absol',        [1],       Tools.getRandomInt(10, 2166),
            [0, 0],     "-800",     "-1400" ),

        new Pokemon( 'Goupix',       [6],        Tools.getRandomInt(10, 757),
            [0, 0],     "-1100",    "-100" ),

        new Pokemon( 'Kaiminus',     [17],        Tools.getRandomInt(10, 970),
            [0, 0],     "-700",     "-600" ),

        new Pokemon( 'Mimigal',      [0, 13],    Tools.getRandomInt(10, 700),
            [0, 0],     "-1600",    "-600" ),

        new Pokemon( 'Amonita',      [15, 17],    Tools.getRandomInt(10, 1324 ),
            [0, 0],     "-1200",    "-500" ),

        new Pokemon( 'Fantominus',   [8, 13],   Tools.getRandomInt(10, 1054 ),
            [0, 0],     "-1600",    "-300" ),

        new Pokemon( 'Tentacool',    [17, 13],    Tools.getRandomInt(10, 892 ),
            [0, 0],     "-2100",    "-200" )
    ];
}

export default POKEMON;