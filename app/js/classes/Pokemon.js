import mapboxgl from "mapbox-gl";
import TYPE from "../TYPE";
import trainer from './Trainer';

export class Pokemon {

    constructor( name, types, pc, position = [0, 0], bgposX, bgposY ) {

        this.$popup = null;
        this.$button = null;

        this.name = name;
        this.types = types;
        this.pc = pc;
        this.position = position;

        this.bgposX = bgposX;
        this.bgposY = bgposY;

        this.$marker = null;
        this.marker = null;
        this.popup = new mapboxgl.Popup({
            offset: 25
        });

    }

    createPopup() {

        this.$popup = document.createElement('div');
        let html = `<h1> ${this.name} </h1><ul>`
            for ( let type_id of this.types ) {

                html += `<li>${TYPE[type_id]}</li>`

            }
        html += `</ul> <h3> ${this.pc} PC</h3>`

        this.$popup.innerHTML = html;

        this.$button = document.createElement('button');
        this.$button.innerText = 'Capturer';

        this.$popup.append( this.$button );

        this.popup
            .setDOMContent( this.$popup );

    }

    catchHim() {

        this.$button.addEventListener('click', () => {

            trainer.catch(this);

        });

    }

    render( map ) {
        this.createPopup();
        this.catchHim();

        this.$marker = document.createElement('div');
        this.$marker.className = 'sprite pokemon';
        this.$marker.style.backgroundPositionX = `${this.bgposX}px`;
        this.$marker.style.backgroundPositionY = `${this.bgposY}px`;
        this.marker = new mapboxgl.Marker(this.$marker);

        this.marker
            .setLngLat( this.position )
            .setPopup( this.popup )
            .addTo( map );
    }

    destroy() {
        this.marker.remove();
    }

}