import {Tools} from "./Tools";
import game from './Game';

class Trainer {

    constructor() {

        this.pokedex = [];

    }

    catch( event ){

        const tryCatch  = Tools.getRandomInt(1, 10);
        if( tryCatch <=7 ) {
            this.pokedex.push( event );
            event.destroy();

            for (let key in game.listPokemons){
                game.listPokemons.splice(key, 1);
            }
        }
        else {

        }

    }

    seePokemon( pokemon, position ) {

        if ( Tools.radar( position.lat, position.lng,
                            pokemon.position.lat, pokemon.position.lng ) <= 0.5 ) {

            pokemon.$marker.classList.add('active');

        }
        else {
            pokemon.$marker.classList.remove('active');
        }

    }

}

export default new Trainer;