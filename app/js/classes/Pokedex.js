import trainer from './Trainer';
import TYPE from "../TYPE";

class Pokedex {

    constructor() {

        this.$list = document.getElementById('list');

    }

    render() {

        let html = ``;
        for ( let pokemon of trainer.pokedex ) {

            html += `
                <div class="pokemon-detail">
                    <span class="pc"> ${pokemon.pc} PC</span>
                    <div class="sprite pokemon active" 
                        style="background-position-x: ${pokemon.bgposX}px; background-position-y: ${pokemon.bgposY}px; "></div>
                    <span class="name">${pokemon.name}</span> 
                </div>
            `

        }

        this.$list.innerHTML = html;

    }

}

export default new Pokedex;